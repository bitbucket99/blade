/**
 * Created by seven on 5/2/2015.
 */
var blade = angular.module('blade');
blade.controller('AccountController', function($scope,$localStorage,User) {
    $scope.settingsList = [
        { text: "Enable news from HotWind", checked: true }
    ];
    $scope.signIn = function(){
        User.save($scope.loginData,function(result){
            $localStorage.userToken= result;
        });

    }
})